<?php 

	// SETUP PARAMETERS
	$params = array(

		// TEST MODE
		"testmode"   		=> "on",

		// LIVE KEYS
		"private_live_key" 	=> "sk_live_xxxxxxxxxxxxxxxxxxxxx",
		"public_live_key"  	=> "pk_live_xxxxxxxxxxxxxxxxxxxxx",

		// TEST KEYS
		"private_test_key" 	=> "sk_test_xxxxxxxxxxxxxxxxxxxxx",
		"public_test_key"  	=> "pk_test_xxxxxxxxxxxxxxxxxxxxx"

	);

	// IF THIS IS A TEST MODE
	if ($params['testmode'] == "on") {
		\Stripe\Stripe::setApiKey($params['private_test_key']);
		$pubkey = $params['public_test_key'];
	} 

	// IF THIS IS PRODUCTION ENVIRONMENT
	else {
		\Stripe\Stripe::setApiKey($params['private_live_key']);
		$pubkey = $params['public_live_key'];
	}

?>
