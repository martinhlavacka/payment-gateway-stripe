<?php
	
	// LIBRARY AND CONFIGURATION
	require 'stripe/stripe.php';
	require 'configuration.php';

	// IF THIS IS A REQUEST TO PAY
	if(isset($_POST['stripeToken'])){

		// STORE VALUES
		$amount_cents 	= htmlspecialchars($_POST['total']) * 100; 	// AMOUNT TO CHARGE (INCLUDING CENTS)
		$description 	= "Transaction identification / description goes here";

		// TRY TO PAY
		try {

			// PAYMENT DETAILS
			$charge = \Stripe\Charge::create(
				array(		 
				  "amount" 			=> $amount_cents,
				  "currency" 		=> "DKK",
				  "source" 			=> $_POST['stripeToken'],
				  "description" 	=> $description
				)			  
			);

			// DISPLAY RESULT
			echo "<pre>";
				print_r($charge);
			echo "</pre>";
	 		
	 		// RESULT MESSAGE
			$result = "Congratulation! Your payment has been received.";

		} 

		// IF PAYMENT FAILED - CARD ERROR
		catch(Stripe_CardError $e) {			
			$error = $e->getMessage();
			$result = "Your payment was declined";
		} 

		// IF PAYMENT FAILED - INVALID REQUEST
		catch (Stripe_InvalidRequestError $e) {
			$result = "Your payment was declined";		  
		} 

		// PAYMENT FAILED - AUTHENTICATION ERROR
		catch (Stripe_AuthenticationError $e) {
			$result = "Your payment was declined";
		} 

		// PAYMENT FAILED - CONNECTION ERROR
		catch (Stripe_ApiConnectionError $e) {
			$result = "Your payment was declined";
		} 

		// PAYMENT DECLINED - STRIPE ERROR
		catch (Stripe_Error $e) {
			$result = "Your payment was declined";
		} 

		// REQUEST DECLINED - FORM ERRORS
		catch (Exception $e) {

			// INVALID ZIP CODE
			if ($e->getMessage() == "zip_check_invalid") {
				$result = "Your payment was declined";
			} 

			// INVALID ADDRESS
			else if ($e->getMessage() == "address_check_invalid") {
				$result = "declined";
			} 

			// INVALID CVC
			else if ($e->getMessage() == "cvc_check_invalid") {
				$result = "declined";
			} 

			// ANY THER ERROR
			else {
				$result = "Your payment was declined";
			}

		}
		
		// DISPLAY RESULT OF THE PAYMENT
		echo "Payment result: " . $result;

		// STOP EXECUTING
		exit;

	}

	// IF THIS IS NOT A REQUEST TO PAY
	else{

		// RETURN ERROR
		http_response_code(500);

		// STOP EXECUTING
		die();

	}

?>