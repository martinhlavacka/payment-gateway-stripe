<?php

	// LIBRARY AND CONFIGURATION
	require 'stripe/stripe.php';
	require 'configuration.php';

?>

<!-- INCLUDE jQUERY -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- FORM -->
<form action="result.php" method="post" id="payform">
	
	<!-- PRICE FOR ONE UNIT -->
	<label for="price">Item price</label>
	<input id="price" value="100" type="number" name="price" onchange="calprice();" placeholder="Insert price">
	
	<!-- QUANTITY -->
	<label for="quantity">Quantity</label>
	<input id="quantity" type="number" name="quantity" onchange="calprice();" value="1">

	<!-- CALCULATION OF TOTAL PRICE -->
	<span>Total price:</span>
	<span id="totalcost">100</span>
	<span id="currency-symbol">DKK</span>

	<!-- HIDDEN INPUTS WITH TOKEN -->
	<input type="hidden" value="" id="tokenkey" name="stripeToken" />

	<!-- HIDDEN INPUT WITH PRICE -->
	<input type="hidden" value="10" id="totalpay" name="total" />

	<!-- BUTTON TO PAY -->
	<button type="submit" id="pay-now">Pay Now</button>

</form>

<!-- CHECKOUT SCRIPT -->
<script src="https://checkout.stripe.com/checkout.js" data-currency="dkk"></script>

<script>

	// PRICE CALCULATION
	function calprice(){

		// STORE VALUES
		var price 		= $("#price").val();
		var quantity 	= $("#quantity").val();

		// MAKE CALCULATION
		var totalcost	=	price * quantity;

		// UPDATE VALUES
		$("#totalcost").text(totalcost);
		$("#totalpay").val(totalcost);

	}

</script>

<script>

	var handler = StripeCheckout.configure({
		
		// PAYMENT WINDOWS SETTINGS
		key: '<?php echo $params['public_test_key']; ?>',
		image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
		locale: 'auto',
		currency: 'dkk',

		token: function(token) {
			$("#tokenkey").val(token.id);
		    $("#payform").submit();
		 }

	});

	// ON PAY NOW CLICK
	document.getElementById('pay-now').addEventListener('click', function(e) {
		
		// STORE TOTAL VALUE TO PAY
		var total = $("#totalcost").text();

		// ADD "00" CENTS
		total = total * 100;

		// OPEN CHECKOUT WINDOWS
		handler.open({

			// MORE SETTINGS
		    name: 'IMA Web Studio',
		    description: 'Online payment gateway',
		    amount: total

		});
	  	
	  	// PREVENT FORM SUBMISSION
	  	e.preventDefault();

	});

	// CLOSE MODAL ON CLISE
	window.addEventListener('popstate', function() {
	  	handler.close();
	});

</script>